package org.efrasiyab

import org.scalatest.WordSpec

class MinHashSpec extends WordSpec with RecommendationBehavior {


  val mockEngine = new MinHashRecEngine(mockModel, 10)

  recommendByUser(mockEngine)
  recommendByItems(mockEngine)
  recommendIrrelevantUsers(mockEngine)


} 
