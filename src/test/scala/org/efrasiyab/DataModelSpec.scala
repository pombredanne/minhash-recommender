package org.efrasiyab

import org.scalatest.WordSpec

class DataModelSpec extends WordSpec {

  val path = getClass.getResource("/test.dat").getPath
  val model = MemoryDataModel(path)
  
  "Example Memory Model" should {
    "contain 5 users" in {
      (model.getUsers === 5)
    }

    "users should be 1,2,3,4,5" in {
      assert(model.getUsers.toList.sorted === List(1,2,3,4,5))
    }

    "user 1 should contain items 12, 32, 99" in {
      assert(model.getItems(1).sorted === List(12, 32, 99))
    }

    "user 5 should contain items 65" in {
      assert(model.getItems(5).sorted === List(65))
    }
  }
}
