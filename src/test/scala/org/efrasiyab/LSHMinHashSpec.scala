package org.efrasiyab

import org.scalatest.WordSpec

class LSHMinHashSpec extends WordSpec with RecommendationBehavior {

  val mockEngine = new LSHMinHashRecEngine(mockModel, 10, 9)

  recommendByUser(mockEngine)
  recommendByItems(mockEngine)
  recommendIrrelevantUsers(mockEngine)

} 
