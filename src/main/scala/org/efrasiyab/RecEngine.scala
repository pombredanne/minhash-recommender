package org.efrasiyab
import scala.collection.mutable.{ PriorityQueue => PQ }
import scala.math.Ordering

trait RecEngine[User, Item] {

  val model: DataModel[User, Item]

  protected def similarCandidates(items: Seq[Item]): Seq[User]
  protected def getSimilarity(userId: User, items: Seq[Item]): Option[Double]

  def recommend(items: Seq[Item], threshold: Double): Seq[Item] = {
    //using priority queue for fast retrival
    val pq = PQ.empty[Similarity]
    for {
      candidate <- similarCandidates(items)
      simRatio <- getSimilarity(candidate, items)
    } if (simRatio > threshold) {
      pq.enqueue(Similarity(candidate, simRatio, model.getItems(candidate)))
    }
    //get all recommendable items by filtering user given data
    pq.dequeueAll.flatMap(_.items).distinct.filter(i => !items.exists(_ == i))

  }

  //recommend to a user which exists in model
  def recommend(user: User, threshold: Double): Seq[Item] = {
    model.getItems(user) match {
      case Nil => Seq.empty
      case x :: xs => recommend(x :: xs, threshold)
    }
  }

  //Use this class for indexing similarities in order
  case class Similarity(alikeUser: User, similarRatio: Double, items: Seq[Item])
  implicit val simOrder = new Ordering[Similarity] {
    def compare(x: Similarity, y: Similarity) = {
      val comp = x.similarRatio - y.similarRatio
      if (comp < 0.0)
        -1
      else if (comp > 0)
        1
      else
        0
    }
  }


}
