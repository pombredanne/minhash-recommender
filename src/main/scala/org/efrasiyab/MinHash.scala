package org.efrasiyab
import scala.collection.BitSet
import scala.util.Random
import scala.util.hashing.{ MurmurHash3 => MM3 }


class MinHashRecEngine(val model: DataModel[Int, Int], numberOfHashes: Int) extends RecEngine[Int, Int] {

  type User = Int
  type Item = Int
  type HashFunc = Item => Int
  type Signature = Seq[Int]
  type EngineCore = Map[User, Signature]

  //Hasg functions required by min hash
  protected val hashes: Seq[HashFunc] = Seq.fill[Int](numberOfHashes)(Random.nextInt).map(random => MM3.mix(random, _: Int))

  //Our trained Recommendaition model
  //we use this model to find similar users
  protected val core = model.getUsers.map(u => (u, createSignature(hashes, model.getItems(u)))).toMap

  private def minHash(func: HashFunc, pref: Seq[Item]): Int = pref.map(func(_)).min

  protected def createSignature(funcs: Seq[HashFunc], pref: Seq[Item]): Signature = funcs.map(minHash(_, pref))

  override protected def getSimilarity(userId1: User, items: Seq[Item]): Option[Double] = {
    val user1Signature:Option[Signature] = core.get(userId1)
    val user2Signature = createSignature(hashes, items)
    user1Signature.map(sig => getSimilarityBySig(sig, user2Signature))
  }

  //compare signatures and accumulate same rows
  private def getSimilarityBySig(user1Signature: Signature, user2Signature: Signature): Double = {
    user1Signature.zip(user2Signature).foldLeft(0.0)((acc, i) => if (i._1 == i._2) acc + 1 else acc) / numberOfHashes
  }

  //There is no LSH Optimization, so we return all users
  override protected def similarCandidates(items: Seq[Item]): Seq[User] = model.getUsers

}

