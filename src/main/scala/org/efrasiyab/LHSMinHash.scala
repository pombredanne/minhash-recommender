package org.efrasiyab
import scala.collection.BitSet
import scala.util.Random
import scala.util.hashing.{ MurmurHash3 => MM3 }

class LSHMinHashRecEngine(override val model: DataModel[Int, Int], numberOfHashes: Int, bands: Int) extends MinHashRecEngine(model, numberOfHashes) {
  require(numberOfHashes > bands)
  val rowSize = (numberOfHashes / bands).toInt

  private def hash(row: Seq[Int]) = MM3.seqHash(row)
  type Bucket = Int

  //We map each row signature to a Bucket of Users
  //This means users in same bucket are most likely similar.
  val bucketMap: Map[Bucket, BitSet] = {
    var map = Map[Int, BitSet]()
    for{
      (user, sig) <- core
      bucket <- signatureToBands(sig)
    } map = map + (bucket -> (map.get(bucket).getOrElse(BitSet()) + user))
    map
  }

  //By user signatures, we are mapping droping users to buckets
  private def signatureToBands(s: Signature): Seq[Int] =
    s.grouped(rowSize)
      .flatMap(row => if (row.size == rowSize) Seq(hash(row)) else Seq.empty)
      .toSeq

  //Compute signature and figure out which users are most likely similar
  override protected def similarCandidates(item: Seq[Item]): Seq[User] = {
    val signature = createSignature(hashes, item)    
    val buckets = signatureToBands(signature)
    val cans = buckets.flatMap{ bucket => bucketMap.get(bucket).getOrElse(BitSet()) }.distinct
    cans
  }

}
